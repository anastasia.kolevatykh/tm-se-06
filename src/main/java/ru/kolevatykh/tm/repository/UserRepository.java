package ru.kolevatykh.tm.repository;

import ru.kolevatykh.tm.entity.User;
import ru.kolevatykh.tm.enumerate.RoleType;
import ru.kolevatykh.tm.util.PasswordHash;

import java.util.*;

public class UserRepository {
    private Map<String, User> userMap;

    public UserRepository() {
        this.userMap = new HashMap<>();
    }

    public List<User> findAll() {
        List<User> userList = new ArrayList<>(userMap.values());
        return userList;
    }

    public User findOneById(String id) {
        return userMap.get(id);
    }

    public User findOneByLogin(String login) {
        for (Map.Entry<String, User> entry : userMap.entrySet()) {
            if (entry.getValue().getLogin().equals(login))
                return entry.getValue();
        }
        return null;
    }

    public void persist(User user) {
        userMap.put(user.getId(), user);
    }

    public void merge(User user) {
        userMap.put(user.getId(), user);
    }

    public void remove(String id) {
        for (Iterator<Map.Entry<String, User>> it = userMap.entrySet().iterator(); it.hasNext(); ) {
            Map.Entry<String, User> entry = it.next();
            User task = entry.getValue();
            if (task.getId().equals(id))
                it.remove();
        }
    }

    public void removeAll() {
        userMap = new HashMap<>();
    }
}
