package ru.kolevatykh.tm.repository;

import ru.kolevatykh.tm.entity.Task;

import java.util.*;

public class TaskRepository {
    private Map<String, Task> taskMap;

    public TaskRepository() {
        this.taskMap = new HashMap<>();
    }

    public List<Task> findAll(String userId) {
        List<Task> taskList = new ArrayList<>();
        for (Map.Entry<String, Task> entry : taskMap.entrySet()) {
            if (entry.getValue().getUserId().equals(userId))
                taskList.add(entry.getValue());
        }
        return taskList;
    }

    public Task findOneById(String userId, String id) {
        for (Map.Entry<String, Task> entry : taskMap.entrySet()) {
            if (entry.getValue().getUserId().equals(userId)
                    && entry.getValue().getId().equals(id)) {
                return entry.getValue();
            }
        }
        return null;
    }

    public Task findOneByName(String userId, String name) {
        for (Map.Entry<String, Task> entry : taskMap.entrySet()) {
            if (entry.getValue().getUserId().equals(userId)
                    && entry.getValue().getName().equals(name)) {
                return entry.getValue();
            }
        }
        return null;
    }

    public void persist(Task task) {
        taskMap.put(task.getId(), task);
    }

    public void merge(Task task) {
        taskMap.put(task.getId(), task);
    }

    public void remove(String userId, String id) {
        for (Iterator<Map.Entry<String, Task>> it = taskMap.entrySet().iterator(); it.hasNext(); ) {
            Map.Entry<String, Task> entry = it.next();
            Task task = entry.getValue();
            if (task.getUserId().equals(userId) && task.getId().equals(id)) {
                it.remove();
            }
        }
    }

    public void removeAll(String userId) {
        for (Iterator<Map.Entry<String, Task>> it = taskMap.entrySet().iterator(); it.hasNext(); ) {
            Map.Entry<String, Task> entry = it.next();
            if (entry.getValue().getUserId().equals(userId)) {
                it.remove();
            }
        }
    }

    public void removeTasksWithProjectId(String userId) {
        for (Iterator<Map.Entry<String, Task>> it = taskMap.entrySet().iterator(); it.hasNext(); ) {
            Map.Entry<String, Task> entry = it.next();
            Task task = entry.getValue();
            if (task.getUserId().equals(userId) && task.getProjectId() != null) {
                it.remove();
            }
        }
    }

    public void removeProjectTasks(String userId, String projectId) {
        for (Iterator<Map.Entry<String, Task>> it = taskMap.entrySet().iterator(); it.hasNext(); ) {
            Map.Entry<String, Task> entry = it.next();
            Task task = entry.getValue();
            if (task.getUserId().equals(userId) && task.getProjectId().equals(projectId)) {
                it.remove();
            }
        }
    }

    public List<Task> findTasksByProjectId(String userId, String projectId) {
        List<Task> taskList = new ArrayList<>();
        for (Map.Entry<String, Task> entry : taskMap.entrySet()) {
            if (entry.getValue().getUserId().equals(userId)
                    && entry.getValue().getProjectId().equals(projectId)) {
                taskList.add(entry.getValue());
            }
        }
        return taskList;
    }

    public void assignToProject(String userId, String id, String projectId) {
        if (taskMap.get(id).getUserId().equals(userId))
            taskMap.get(id).setProjectId(projectId);
    }
}
