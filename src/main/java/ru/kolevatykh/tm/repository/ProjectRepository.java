package ru.kolevatykh.tm.repository;

import ru.kolevatykh.tm.entity.Project;

import java.util.*;

public class ProjectRepository {
    private Map<String, Project> projectMap;

    public ProjectRepository() {
        this.projectMap = new HashMap<>();
    }

    public List<Project> findAll(String userId) {
        List<Project> projectList = new ArrayList<>();
        for (Map.Entry<String, Project> entry : projectMap.entrySet()) {
            if (entry.getValue().getUserId().equals(userId))
                projectList.add(entry.getValue());
        }
        return projectList;
    }

    public Project findOneById(String userId, String id) {
        for (Map.Entry<String, Project> entry : projectMap.entrySet()) {
            if (entry.getValue().getUserId().equals(userId)
                    && entry.getValue().getId().equals(id))
                return entry.getValue();
        }
        return null;
    }

    public Project findOneByName(String userId, String name) {
        for (Map.Entry<String, Project> entry : projectMap.entrySet()) {
            if (entry.getValue().getUserId().equals(userId)
                    && entry.getValue().getName().equals(name))
                return entry.getValue();
        }
        return null;
    }

    public void persist(Project project) {
        projectMap.put(project.getId(), project);
    }

    public void merge(Project project) {
        projectMap.put(project.getId(), project);
    }

    public void remove(String userId, String id) {
        for (Iterator<Map.Entry<String, Project>> it = projectMap.entrySet().iterator(); it.hasNext(); ) {
            Map.Entry<String, Project> entry = it.next();
            if (entry.getValue().getUserId().equals(userId) && entry.getKey().equals(id)) {
                it.remove();
            }
        }
    }

    public void removeAll(String userId) {
        for (Iterator<Map.Entry<String, Project>> it = projectMap.entrySet().iterator(); it.hasNext(); ) {
            Map.Entry<String, Project> entry = it.next();
            if (entry.getValue().getUserId().equals(userId)) {
                it.remove();
            }
        }
    }
}
