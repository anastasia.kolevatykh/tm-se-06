package ru.kolevatykh.tm;

import ru.kolevatykh.tm.bootstrap.Bootstrap;
import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.command.general.*;
import ru.kolevatykh.tm.command.project.*;
import ru.kolevatykh.tm.command.task.*;
import ru.kolevatykh.tm.command.user.*;

public class Application {
    private static AbstractCommand[] commandInstances = {
            new HelpCommand(),
            new ExitCommand(),

            new ProjectCreateCommand(),
            new ProjectListCommand(),
            new ProjectShowCommand(),
            new ProjectUpdateCommand(),
            new ProjectRemoveCommand(),
            new ProjectClearCommand(),

            new TaskCreateCommand(),
            new TaskListCommand(),
            new TaskAssignCommand(),
            new TaskUpdateCommand(),
            new TaskRemoveCommand(),
            new TaskClearCommand(),

            new UserAuthorizeCommand(),
            new UserDeleteCommand(),
            new UserEditCommand(),
            new UserListCommand(),
            new UserLogOutCommand(),
            new UserRegisterCommand(),
            new UserShowCommand()
    };

    public static void main(String[] args) {
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.init(commandInstances);
    }
}
