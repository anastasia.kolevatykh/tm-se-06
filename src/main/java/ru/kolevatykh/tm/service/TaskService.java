package ru.kolevatykh.tm.service;

import ru.kolevatykh.tm.entity.Task;
import ru.kolevatykh.tm.repository.TaskRepository;

import java.util.List;

public class TaskService {
    private TaskRepository taskRepository;

    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public void persist(Task task) {
        if (task == null) return;
        taskRepository.persist(task);
    }

    public List<Task> findAll(String userId) {
        if (userId == null || userId.isEmpty()) return null;
        List<Task> taskList = taskRepository.findAll(userId);
        if (taskList.isEmpty()) return null;
        return taskList;
    }

    public List<Task> showProjectTasks(String userId, String projectId) {
        if (userId == null || userId.isEmpty()
                || projectId == null || projectId.isEmpty()) return null;
        List<Task> taskList = taskRepository.findTasksByProjectId(userId, projectId);
        if (taskList.isEmpty()) return null;
        return taskList;
    }

    public Task findOneByName(String userId, String name) {
        if (userId == null || userId.isEmpty() || name == null || name.isEmpty()) return null;
        return taskRepository.findOneByName(userId, name);
    }

    public void assignToProject(String userId, String id, String projectId) {
        if (userId == null || userId.isEmpty()
                || id == null || id.isEmpty()
                || projectId == null || projectId.isEmpty()) return;
        taskRepository.assignToProject(userId, id, projectId);
    }

    public void merge(Task task) {
        if (task == null) return;
        taskRepository.merge(task);
    }

    public void removeAll(String userId) {
        if (userId == null || userId.isEmpty()) return;
        taskRepository.removeAll(userId);
    }

    public void removeTasksWithProjectId(String userId) {
        if (userId == null || userId.isEmpty()) return;
        taskRepository.removeTasksWithProjectId(userId);
    }

    public void remove(String userId, String id) {
        if (userId == null || userId.isEmpty() || id == null || id.isEmpty()) return;
        taskRepository.remove(userId, id);
    }

    public void removeProjectTasks(String userId, String projectId) {
        if (userId == null || userId.isEmpty() || projectId == null || projectId.isEmpty()) return;

        taskRepository.removeProjectTasks(userId, projectId);
    }
}
