package ru.kolevatykh.tm.service;

import ru.kolevatykh.tm.entity.Project;
import ru.kolevatykh.tm.repository.ProjectRepository;

import java.util.List;

public class ProjectService {
    private ProjectRepository projectRepository;

    public ProjectService(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    public void persist(Project project) {
        if (project == null) return;
        projectRepository.persist(project);
    }

    public List<Project> findAll(String userId) {
        if (userId == null || userId.isEmpty()) return null;
        List<Project> projectList = projectRepository.findAll(userId);
        if (projectList.isEmpty()) return null;
        return projectList;
    }

    public Project findByName(String userId, String name) {
        if (name == null || name.isEmpty() || userId == null || userId.isEmpty()) return null;
        return projectRepository.findOneByName(userId, name);
    }

    public void merge(Project project) {
        if (project == null) return;
        projectRepository.merge(project);
    }

    public void remove(String userId, String id) {
        if (userId == null || userId.isEmpty() || id == null || id.isEmpty()) return;
        projectRepository.remove(userId, id);
    }

    public void removeAll(String userId) {
        if (userId == null || userId.isEmpty()) return;
        projectRepository.removeAll(userId);
    }
}
