package ru.kolevatykh.tm.service;

import ru.kolevatykh.tm.entity.User;
import ru.kolevatykh.tm.enumerate.RoleType;
import ru.kolevatykh.tm.repository.UserRepository;
import ru.kolevatykh.tm.util.PasswordHash;

import java.util.List;

public class UserService {
    private UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public List<User> findAll() {
        return userRepository.findAll();
    }

    public User findOneById(String id) {
        if (id == null || id.isEmpty()) return null;
        return userRepository.findOneById(id);
    }

    public User findOneByLogin(String login) {
        if (login == null || login.isEmpty()) return null;
        return userRepository.findOneByLogin(login);
    }

    public void persist(User user) {
        if (user == null) return;
        userRepository.persist(user);
    }

    public void merge(User user) {
        if (user == null) return;
        userRepository.merge(user);
    }

    public void remove(String id) {
        if (id == null || id.isEmpty()) return;
        userRepository.remove(id);
    }

    public void removeAll() {
        userRepository.removeAll();
    }

    public void createTestUsers() {
        User admin = new User("admin", PasswordHash.getPasswordHash("pass1"), RoleType.ADMIN);
        User user = new User("user", PasswordHash.getPasswordHash("pass2"), RoleType.USER);
        userRepository.persist(admin);
        userRepository.persist(user);
    }
}
