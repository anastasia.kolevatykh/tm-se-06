package ru.kolevatykh.tm.bootstrap;

import ru.kolevatykh.tm.enumerate.RoleType;
import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.entity.User;
import ru.kolevatykh.tm.repository.ProjectRepository;
import ru.kolevatykh.tm.repository.TaskRepository;
import ru.kolevatykh.tm.repository.UserRepository;
import ru.kolevatykh.tm.service.ProjectService;
import ru.kolevatykh.tm.service.TaskService;
import ru.kolevatykh.tm.service.UserService;

import java.util.*;

public class Bootstrap {
    private User currentUser;
    private Scanner scanner = new Scanner(System.in);
    private UserRepository userRepository = new UserRepository();
    private ProjectRepository projectRepository = new ProjectRepository();
    private TaskRepository taskRepository = new TaskRepository();
    private UserService userService = new UserService(userRepository);
    private ProjectService projectService = new ProjectService(projectRepository);
    private TaskService taskService = new TaskService(taskRepository);
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    public Scanner getScanner() {
        return scanner;
    }

    public void setUser(User user) {
        this.currentUser = user;
    }

    public User getUser() {
        return currentUser;
    }

    public UserService getUserService() {
        return userService;
    }

    public ProjectService getProjectService() {
        return projectService;
    }

    public TaskService getTaskService() {
        return taskService;
    }

    private void registryCommand(final AbstractCommand command) throws Exception {
        final String commandName = command.getName();
        final String commandShortName = command.getShortName();
        final String commandDescription = command.getDescription();

        if (commandName == null || commandName.isEmpty()) {
            throw new Exception("There's no such command name.");
        }
        if (commandDescription == null || commandDescription.isEmpty()) {
            throw new Exception("There's no such command description.");
        }
        command.setBootstrap(this);
        commands.put(commandName, command);

        if (commandShortName != null && !commandShortName.isEmpty()) {
            commands.put(commandShortName, command);
        }
    }

    private void start() {
        System.out.println("*** Welcome to task manager ***"
                + "\nType \"help\" for details.");
        String command = "";
        while (!"exit".equals(command)) {
            command = scanner.nextLine();
            try {
                execute(command);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }

    private void execute(final String command) throws Exception {
        if (command == null || command.isEmpty()) return;
        final AbstractCommand abstractCommand = commands.get(command);
        if (abstractCommand == null) return;

        if (abstractCommand.needAuth() && currentUser == null) {
            throw new Exception("[Command is not allowed. You need to authorize or register.]");
        }
        if (abstractCommand.needAdminRole() && !getUser().getRoleType().equals(RoleType.ADMIN)) {
            throw new Exception("[Command is not allowed. You need Admin role for this command.]");
        }
        abstractCommand.execute();
    }

    public Collection<AbstractCommand> getCommands() {
        return new ArrayList<>(commands.values());
    }

    public void init(AbstractCommand... commandInstances) {
        try {
            for (AbstractCommand command : commandInstances) {
                registryCommand(command);
            }
            userService.createTestUsers();
            start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
