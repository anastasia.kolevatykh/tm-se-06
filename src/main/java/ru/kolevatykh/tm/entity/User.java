package ru.kolevatykh.tm.entity;

import ru.kolevatykh.tm.enumerate.RoleType;
import ru.kolevatykh.tm.util.PasswordHash;

import java.util.UUID;

public class User {
    private String id = UUID.randomUUID().toString();
    private String login;
    private String passwordHash;
    private RoleType roleType;
    private boolean auth = false;

    public User() {
    }

    public User(String login, String password) {
        this();
        this.login = login;
        this.passwordHash = password;
    }

    public User(String login, String password, RoleType roleType) {
        this();
        this.login = login;
        this.passwordHash = password;
        this.roleType = roleType;
    }

    public String getId() {
        return id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String password) {
        this.passwordHash = PasswordHash.getPasswordHash(password);
    }

    public RoleType getRoleType() {
        return roleType;
    }

    public void setRoleType(RoleType roleType) {
        this.roleType = roleType;
    }

    public boolean isAuth() {
        return auth;
    }

    public void setAuth(boolean auth) {
        this.auth = auth;
    }

    @Override
    public String toString() {
        return "id: '" + id + '\'' +
                ", login: '" + login + '\'' +
                ", roleType: " + roleType +
                ", isAuth: " + isAuth();
    }
}
