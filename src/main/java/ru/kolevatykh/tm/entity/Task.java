package ru.kolevatykh.tm.entity;

import java.util.Date;
import java.util.UUID;

public class Task {
    private String userId;
    private String projectId;
    private String id = UUID.randomUUID().toString();
    private String name;
    private String description;
    private Date startDate;
    private Date endDate;

    public Task() {
    }

    public Task(String name, String description, Date startDate, Date endDate) {
        this();
        this.name = name;
        this.description = description;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    @Override
    public String toString() {
        return "user id: '" + userId + '\'' +
                ", project id: '" + projectId + '\'' +
                ", id: '" + id + '\'' +
                ", name: '" + name + '\'' +
                ", description: '" + description + '\'' +
                ", startDate: " + startDate +
                ", endDate: " + endDate;
    }
}
