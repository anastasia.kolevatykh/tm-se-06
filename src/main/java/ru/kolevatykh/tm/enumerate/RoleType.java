package ru.kolevatykh.tm.enumerate;

public enum RoleType {
    ADMIN("Admin"),
    USER("User");

    private final String roleName;

    RoleType(String roleName) {
        this.roleName = roleName;
    }

    public String displayName() {
        return roleName;
    }
}
