package ru.kolevatykh.tm.command.project;

import ru.kolevatykh.tm.command.AbstractCommand;

public class ProjectRemoveCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "project-remove";
    }

    @Override
    public String getShortName() {
        return "pr";
    }

    @Override
    public String getDescription() {
        return "Remove selected project.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public boolean needAdminRole() {
        return false;
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT REMOVE]\nEnter project name: ");
        String name = bootstrap.getScanner().nextLine();

        String userId = bootstrap.getUser().getId();
        String projectId = bootstrap.getProjectService().findByName(userId, name).getId();

        if (projectId == null) {
            System.out.println("[The project '" + name + "' does not exist!]");
        } else {
            bootstrap.getTaskService().removeProjectTasks(userId, projectId);
            bootstrap.getProjectService().remove(userId, projectId);
            System.out.println("[Removed project with tasks.]\n[OK]");
        }
    }
}
