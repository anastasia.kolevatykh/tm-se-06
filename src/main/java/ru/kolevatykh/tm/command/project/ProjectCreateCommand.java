package ru.kolevatykh.tm.command.project;

import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.entity.Project;
import ru.kolevatykh.tm.util.DateFormatter;

import java.util.Date;

public class ProjectCreateCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "project-create";
    }

    @Override
    public String getShortName() {
        return "pcr";
    }

    @Override
    public String getDescription() {
        return "Create new project.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public boolean needAdminRole() {
        return false;
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT CREATE]\nEnter project name: ");
        String name = bootstrap.getScanner().nextLine();

        System.out.println("Enter project description: ");
        String description = bootstrap.getScanner().nextLine();

        System.out.println("Enter project start date: ");
        String projectStartDate = bootstrap.getScanner().next();
        Date startDate = DateFormatter.parseDate(projectStartDate);

        System.out.println("Enter project end date: ");
        String projectEndDate = bootstrap.getScanner().next();
        Date endDate = DateFormatter.parseDate(projectEndDate);

        Project project = new Project(name, description, startDate, endDate);
        project.setUserId(bootstrap.getUser().getId());

        bootstrap.getProjectService().persist(project);
        System.out.println("[OK]");
    }
}
