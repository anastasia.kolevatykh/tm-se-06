package ru.kolevatykh.tm.command.project;

import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.entity.Project;
import ru.kolevatykh.tm.util.DateFormatter;

import java.util.Date;

public class ProjectUpdateCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "project-update";
    }

    @Override
    public String getShortName() {
        return "pu";
    }

    @Override
    public String getDescription() {
        return "Update selected project.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public boolean needAdminRole() {
        return false;
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT UPDATE]\nEnter project name: ");
        String name = bootstrap.getScanner().nextLine();

        String userId = bootstrap.getUser().getId();
        String id = bootstrap.getProjectService().findByName(userId, name).getId();

        if (id == null) {
            System.out.println("[The project '" + name + "' does not exist!]");
            return;
        }

        System.out.println("Enter new project name: ");
        String nameNew = bootstrap.getScanner().nextLine();

        System.out.println("Enter new project description: ");
        String descriptionNew = bootstrap.getScanner().nextLine();

        System.out.println("Enter project start date: ");
        String startNew = bootstrap.getScanner().nextLine();
        Date startDateNew = DateFormatter.parseDate(startNew);

        System.out.println("Enter project end date: ");
        String endNew = bootstrap.getScanner().nextLine();
        Date endDateNew = DateFormatter.parseDate(endNew);

        Project project = bootstrap.getProjectService().findByName(userId, name);
        project.setName(nameNew);
        project.setDescription(descriptionNew);
        project.setStartDate(startDateNew);
        project.setEndDate(endDateNew);

        bootstrap.getProjectService().merge(project);
        System.out.println("[OK]");
    }
}
