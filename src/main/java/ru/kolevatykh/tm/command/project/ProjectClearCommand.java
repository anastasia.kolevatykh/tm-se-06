package ru.kolevatykh.tm.command.project;

import ru.kolevatykh.tm.command.AbstractCommand;

public class ProjectClearCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "project-clear";
    }

    @Override
    public String getShortName() {
        return "pcl";
    }

    @Override
    public String getDescription() {
        return "\tRemove all projects.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public boolean needAdminRole() {
        return false;
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT CLEAR]");
        String userId = bootstrap.getUser().getId();
        bootstrap.getTaskService().removeTasksWithProjectId(userId);
        bootstrap.getProjectService().removeAll(userId);
        System.out.println("[Removed all projects with tasks.]\n[OK]");
    }
}
