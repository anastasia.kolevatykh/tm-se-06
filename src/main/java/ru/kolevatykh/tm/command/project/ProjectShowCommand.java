package ru.kolevatykh.tm.command.project;

import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.entity.Task;

import java.util.List;

public class ProjectShowCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "project-show";
    }

    @Override
    public String getShortName() {
        return "psh";
    }

    @Override
    public String getDescription() {
        return "\tShow tasks of selected project.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public boolean needAdminRole() {
        return false;
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT SHOW]\nEnter project name: ");
        String projectName = bootstrap.getScanner().nextLine();

        String userId = bootstrap.getUser().getId();
        String projectId = bootstrap.getProjectService().findByName(userId, projectName).getId();

        if (projectId == null) {
            System.out.println("[The project '" + projectName + "' does not exist!]");
        } else {
            List<Task> taskList = bootstrap.getTaskService().showProjectTasks(userId, projectId);

            if (taskList != null) {
                System.out.println("[TASK LIST]");

                StringBuilder projectTasks = new StringBuilder();
                int i = 0;

                for (Task task : taskList) {
                    if (projectId.equals(task.getProjectId())) {
                        projectTasks
                                .append(++i)
                                .append(". ")
                                .append(task.toString())
                                .append(System.lineSeparator());
                    }
                }

                String taskString = projectTasks.toString();
                System.out.println(taskString);
            } else {
                System.out.println("No tasks yet.");
            }
        }
    }
}
