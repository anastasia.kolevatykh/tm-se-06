package ru.kolevatykh.tm.command.user;

import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.entity.User;

public class UserEditCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "user-edit";
    }

    @Override
    public String getShortName() {
        return "ue";
    }

    @Override
    public String getDescription() {
        return "\t\tUpdate login or password.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public boolean needAdminRole() {
        return false;
    }

    @Override
    public void execute() {
        System.out.println("[USER EDIT]\nEnter your login for edit: ");
        String login = bootstrap.getScanner().nextLine();

        User user = bootstrap.getUserService().findOneByLogin(login);

        if (user == null) {
            System.out.println("[The login '" + login + "' does not exist!]");
            return;
        }

        String id = user.getId();

        System.out.println("Enter new login: ");
        String loginNew = bootstrap.getScanner().nextLine();

        System.out.println("Enter new password: ");
        String passwordNew = bootstrap.getScanner().nextLine();

        user.setLogin(loginNew);
        user.setPasswordHash(passwordNew);
        bootstrap.getUserService().merge(user);
        System.out.println("[OK]");
    }
}
