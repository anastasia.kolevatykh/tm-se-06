package ru.kolevatykh.tm.command.user;

import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.entity.User;

import java.util.List;

public class UserListCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "user-list";
    }

    @Override
    public String getShortName() {
        return "uli";
    }

    @Override
    public String getDescription() {
        return "\t\tShow all users.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public boolean needAdminRole() {
        return true;
    }

    @Override
    public void execute() {
        System.out.println("[USER LIST]");
        List<User> userList = bootstrap.getUserService().findAll();

        if (userList == null) {
            System.out.println("No users yet.");
        } else {
            StringBuilder users = new StringBuilder();
            int i = 0;

            for (User user : userList) {
                users
                        .append(++i)
                        .append(". ")
                        .append(user.toString())
                        .append(System.lineSeparator());
            }

            String userString = users.toString();
            System.out.println(userString);
        }
    }
}
