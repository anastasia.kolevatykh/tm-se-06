package ru.kolevatykh.tm.command.user;

import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.entity.User;
import ru.kolevatykh.tm.util.PasswordHash;

public class UserAuthorizeCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "user-authorize";
    }

    @Override
    public String getShortName() {
        return "ua";
    }

    @Override
    public String getDescription() {
        return "Sign in.";
    }

    @Override
    public boolean needAuth() {
        return false;
    }

    @Override
    public boolean needAdminRole() {
        return false;
    }

    @Override
    public void execute() {
        User curUser = bootstrap.getUser();

        if (curUser != null) {
            System.out.println("[You authorized under login: " + curUser.getLogin()
                    + "\nPlease LOGOUT first, in order to authorize under OTHER account.]");
            return;
        }

        System.out.println("[USER AUTHORIZATION]\nEnter your login: ");
        String login = bootstrap.getScanner().nextLine();

        User user = bootstrap.getUserService().findOneByLogin(login);

        if (user == null) {
            System.out.println("[The login '" + login + "' does not exist. Please, retry or register.]");
            return;
        }

        System.out.println("Enter your password: ");
        String password = bootstrap.getScanner().nextLine();

        if (!user.getPasswordHash().equals(PasswordHash.getPasswordHash(password))) {
            System.out.println("[Wrong password.]");
            return;
        }

        user.setAuth(true);
        bootstrap.setUser(user);
        System.out.println("[OK]");
    }
}
