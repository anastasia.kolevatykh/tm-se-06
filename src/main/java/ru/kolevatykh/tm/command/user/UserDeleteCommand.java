package ru.kolevatykh.tm.command.user;

import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.entity.User;

public class UserDeleteCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "user-delete";
    }

    @Override
    public String getShortName() {
        return "ud";
    }

    @Override
    public String getDescription() {
        return "\tDelete account.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public boolean needAdminRole() {
        return false;
    }

    @Override
    public void execute() {
        System.out.println("[USER DELETE ACCOUNT]");
        System.out.println("Are you sure you want to delete your account? y/n");
        String answer = bootstrap.getScanner().nextLine();

        if (answer.equals("y")) {
            System.out.println("Enter user login: ");
            String login = bootstrap.getScanner().nextLine();

            User user = bootstrap.getUserService().findOneByLogin(login);

            if (user == null) {
                System.out.println("[The user '" + login + "' does not exist!]");
            } else {
                String userId = bootstrap.getUser().getId();

                bootstrap.getTaskService().removeAll(userId);
                bootstrap.getProjectService().removeAll(userId);
                bootstrap.getUserService().remove(userId);
                bootstrap.setUser(null);
                System.out.println("[Deleted user account with projects and tasks.]\n[OK]");
            }
        }
    }
}
