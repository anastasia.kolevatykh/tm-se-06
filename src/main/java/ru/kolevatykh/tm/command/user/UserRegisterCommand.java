package ru.kolevatykh.tm.command.user;

import ru.kolevatykh.tm.enumerate.RoleType;
import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.entity.User;

public class UserRegisterCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "user-register";
    }

    @Override
    public String getShortName() {
        return "ur";
    }

    @Override
    public String getDescription() {
        return "\tRegister new user.";
    }

    @Override
    public boolean needAuth() {
        return false;
    }

    @Override
    public boolean needAdminRole() {
        return false;
    }

    @Override
    public void execute() {
        User curUser = bootstrap.getUser();

        if (curUser != null) {
            System.out.println("[You authorized under login: " + curUser.getLogin()
                    + "\nPlease LOGOUT first, in order to register new account.]");
            return;
        }

        System.out.println("[USER REGISTRATION]\nEnter your login: ");
        String login = bootstrap.getScanner().nextLine();

        User user = bootstrap.getUserService().findOneByLogin(login);

        if (user != null) {
            System.out.println("[Login '" + login + "' already exists. Please, retry.]");
            return;
        }

        System.out.println("Enter your password: ");
        String password = bootstrap.getScanner().nextLine();

        user = new User();
        user.setLogin(login);
        user.setPasswordHash(password);
        user.setRoleType(RoleType.USER);
        user.setAuth(true);

        bootstrap.getUserService().persist(user);
        bootstrap.setUser(user);
        System.out.println("[OK]");
    }
}
