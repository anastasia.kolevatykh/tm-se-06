package ru.kolevatykh.tm.command.user;

import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.entity.User;

public class UserLogOutCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "user-logout";
    }

    @Override
    public String getShortName() {
        return "ulo";
    }

    @Override
    public String getDescription() {
        return "\tLogout from account.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public boolean needAdminRole() {
        return false;
    }

    @Override
    public void execute() {
        System.out.println("[USER LOGOUT]\nEnter login to confirm logout: ");
        String login = bootstrap.getScanner().nextLine();

        User user = bootstrap.getUserService().findOneByLogin(login);

        if (user == null) {
            System.out.println("[The login '" + login + "' does not exist!]");
            return;
        }

        String id = bootstrap.getUser().getId();

        System.out.println("Confirm logout, y/n: ");
        String answer = bootstrap.getScanner().nextLine();

        if (answer.equals("y")) {
            user.setAuth(false);
            bootstrap.setUser(null);
            System.out.println("[OK]");
        }
    }
}
