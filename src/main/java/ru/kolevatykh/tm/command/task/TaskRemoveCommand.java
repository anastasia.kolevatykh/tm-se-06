package ru.kolevatykh.tm.command.task;

import ru.kolevatykh.tm.command.AbstractCommand;

public class TaskRemoveCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "task-remove";
    }

    @Override
    public String getShortName() {
        return "tr";
    }

    @Override
    public String getDescription() {
        return "\tRemove selected task.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public boolean needAdminRole() {
        return false;
    }

    @Override
    public void execute() {
        System.out.println("[TASK REMOVE]\nEnter task name: ");
        String taskName = bootstrap.getScanner().nextLine();

        String userId = bootstrap.getUser().getId();
        String taskId = bootstrap.getTaskService().findOneByName(userId, taskName).getId();

        if (taskId == null) {
            System.out.println("[The task '" + taskName + "' does not exist!]");
        }

        bootstrap.getTaskService().remove(userId, taskId);
        System.out.println("[OK]");
    }
}
