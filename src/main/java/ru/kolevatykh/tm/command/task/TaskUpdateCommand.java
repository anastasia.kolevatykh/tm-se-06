package ru.kolevatykh.tm.command.task;

import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.entity.Task;
import ru.kolevatykh.tm.util.DateFormatter;

import java.util.Date;

public class TaskUpdateCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "task-update";
    }

    @Override
    public String getShortName() {
        return "tu";
    }

    @Override
    public String getDescription() {
        return "\tUpdate selected task.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public boolean needAdminRole() {
        return false;
    }

    @Override
    public void execute() {
        System.out.println("[TASK UPDATE]\nEnter task name: ");
        String name = bootstrap.getScanner().nextLine();

        String userId = bootstrap.getUser().getId();
        String id = bootstrap.getTaskService().findOneByName(userId, name).getId();

        if (id == null) {
            System.out.println("[The task '" + name + "' does not exist!]");
            return;
        }

        System.out.println("Enter new task name: ");
        String nameNew = bootstrap.getScanner().nextLine();

        System.out.println("Enter new task description: ");
        String descriptionNew = bootstrap.getScanner().nextLine();

        System.out.println("Enter new task start date: ");
        String startNew = bootstrap.getScanner().nextLine();
        Date startDateNew = DateFormatter.parseDate(startNew);

        System.out.println("Enter new task end date: ");
        String endNew = bootstrap.getScanner().nextLine();
        Date endDateNew = DateFormatter.parseDate(endNew);

        Task task = bootstrap.getTaskService().findOneByName(userId, name);
        task.setName(nameNew);
        task.setDescription(descriptionNew);
        task.setStartDate(startDateNew);
        task.setEndDate(endDateNew);

        bootstrap.getTaskService().merge(task);
        System.out.println("[OK]");
    }
}
