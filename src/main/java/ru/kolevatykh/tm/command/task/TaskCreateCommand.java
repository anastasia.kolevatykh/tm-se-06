package ru.kolevatykh.tm.command.task;

import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.entity.Task;
import ru.kolevatykh.tm.util.DateFormatter;

import java.util.Date;

public class TaskCreateCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "task-create";
    }

    @Override
    public String getShortName() {
        return "tcr";
    }

    @Override
    public String getDescription() {
        return "\tCreate new task.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public boolean needAdminRole() {
        return false;
    }

    @Override
    public void execute() {
        System.out.println("[TASK CREATE]\nEnter task name: ");
        String name = bootstrap.getScanner().nextLine();

        System.out.println("Enter task description: ");
        String description = bootstrap.getScanner().nextLine();

        System.out.println("Enter task start date: ");
        String taskStartDate = bootstrap.getScanner().next();
        Date startDate = DateFormatter.parseDate(taskStartDate);

        System.out.println("Enter task end date: ");
        String taskEndDate = bootstrap.getScanner().next();
        Date endDate = DateFormatter.parseDate(taskEndDate);

        Task task = new Task(name, description, startDate, endDate);
        task.setUserId(bootstrap.getUser().getId());

        bootstrap.getTaskService().persist(task);
        System.out.println("[OK]");
    }
}
