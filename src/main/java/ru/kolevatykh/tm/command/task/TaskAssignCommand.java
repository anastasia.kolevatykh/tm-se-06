package ru.kolevatykh.tm.command.task;

import ru.kolevatykh.tm.command.AbstractCommand;

public class TaskAssignCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "task-assign";
    }

    @Override
    public String getShortName() {
        return "ta";
    }

    @Override
    public String getDescription() {
        return "\tAssign task to project.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public boolean needAdminRole() {
        return false;
    }

    @Override
    public void execute() {
        System.out.println("[ASSIGN TASK TO PROJECT]\nEnter task name:");
        String taskName = bootstrap.getScanner().nextLine();

        String userId = bootstrap.getUser().getId();
        String taskId = bootstrap.getTaskService().findOneByName(userId, taskName).getId();

        if (taskId == null) {
            System.out.println("[The task '" + taskName + "' does not exist!]");
            return;
        }

        System.out.println("Enter project name:");
        String projectName = bootstrap.getScanner().nextLine();

        String projectId = bootstrap.getProjectService().findByName(userId, projectName).getId();

        if (projectId == null) {
            System.out.println("[The project '" + projectName + "' does not exist!]");
            return;
        }

        bootstrap.getTaskService().assignToProject(userId, taskId, projectId);
        System.out.println("[OK]");
    }
}
