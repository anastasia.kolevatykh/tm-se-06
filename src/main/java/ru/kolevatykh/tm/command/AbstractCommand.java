package ru.kolevatykh.tm.command;

import ru.kolevatykh.tm.bootstrap.Bootstrap;

public abstract class AbstractCommand {
    protected Bootstrap bootstrap;

    public AbstractCommand() {
    }

    public AbstractCommand(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void setBootstrap(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public abstract String getName();

    public abstract String getShortName();

    public abstract String getDescription();

    public abstract boolean needAuth();

    public abstract boolean needAdminRole();

    public abstract void execute() throws Exception;
}
