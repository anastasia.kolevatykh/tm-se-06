package ru.kolevatykh.tm.command.general;

import ru.kolevatykh.tm.command.AbstractCommand;

public class ExitCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "exit";
    }

    @Override
    public String getShortName() {
        return "e";
    }

    @Override
    public String getDescription() {
        return "\t\t\tExit.";
    }

    @Override
    public boolean needAuth() {
        return false;
    }

    @Override
    public boolean needAdminRole() {
        return false;
    }

    @Override
    public void execute() {
        System.out.println("[Goodbye!]");
        System.exit(0);
    }
}
