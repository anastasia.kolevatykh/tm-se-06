package ru.kolevatykh.tm.command.general;

import ru.kolevatykh.tm.command.AbstractCommand;

public class HelpCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "help";
    }

    @Override
    public String getShortName() {
        return "h";
    }

    @Override
    public String getDescription() {
        return "\t\t\tShow all commands.";
    }

    @Override
    public boolean needAuth() {
        return false;
    }

    @Override
    public boolean needAdminRole() {
        return false;
    }

    @Override
    public void execute() {
        for (final AbstractCommand command : bootstrap.getCommands()) {
            System.out.println(command.getName() + ": " + command.getDescription());
        }
    }
}
