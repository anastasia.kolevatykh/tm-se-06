package ru.kolevatykh.tm.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateFormatter {
    private static SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");

    public static Date parseDate(final String dateInput) {
        try {
            return dateFormat.parse(dateInput);
        } catch (ParseException pe) {
            System.out.println("Parse date exception: Wrong date format, for instance 30.1.2020.");
            return null;
        }
    }
}
